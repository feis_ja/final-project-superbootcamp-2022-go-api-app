package models


type Game struct {
	ID          		uint       	`gorm:"primary_key" json:"id"`
	Poster				string		`json:"poster"`
	PosterCaption		string		`json:"poster_caption"`
	Logo				string		`json:"logo"`
	Title       		string     	`json:"title"`
	Price       		uint       	`json:"price"`
	Developer      		string     	`json:"developer"`
	Publisher       	string    	`json:"publisher"`
	ReleaseDate 		string     	`json:"release_date"`
	ShortDescription	string		`json:"short_description"`
	Description			string		`json:"description"`
	Purchases  			[]Purchase 	
	Wishlists  			[]Wishlist 	
	GameGenres			[]GameGenre		
	GameFeatures		[]GameFeature	
	GamePlatforms		[]GamePlatform	
	GameImages			[]GameImage		
} 