package models

import (
	"html"
    "strings"
    "time"

    "api-app/utils/token"

    "golang.org/x/crypto/bcrypt"
    "gorm.io/gorm"
)

type User struct {
	ID          	uint      		`json:"id" gorm:"primary_key;not null;uniqueIndex" `
	Username        string    		`json:"username" gorm:"not null;uniqueIndex"`
	Email 			string    		`json:"email" gorm:"not null;uniqueIndex"`	
	Password		string			`json:"Password" gorm:"not null"`
	CreatedAt 		time.Time 		`json:"created_at"`
    UpdatedAt 		time.Time 		`json:"updated_at"`
	UserRoleID		int 	  		`json:"user_role_id" gorm:"default:1"`
	UserRole		UserRole 		`json:"-"`
	Purchases		[]Purchase		`json:"-"`
    Wishlists		[]Wishlist	    `json:"-"`

	
}

func VerifyPassword(password, hashedPassword string) error {
    return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
}

func LoginCheck(username string, password string, db *gorm.DB) (string, error) {

    var err error

    u := User{}

    err = db.Model(User{}).Where("username = ?", username).Take(&u).Error

    if err != nil {
        return "", err
    }

    err = VerifyPassword(password, u.Password)

    if err != nil && err == bcrypt.ErrMismatchedHashAndPassword {
        return "", err
    }

    token, err := token.GenerateToken(u.ID)

    if err != nil {
        return "", err
    }

    return token, nil

}

func (u *User) SaveUser(db *gorm.DB) (*User, error) {
    //turn password into hash
    hashedPassword, errPassword := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
    if errPassword != nil {
        return &User{}, errPassword
    }
    u.Password = string(hashedPassword)
    //remove spaces in username
    u.Username = html.EscapeString(strings.TrimSpace(u.Username))

    var err error = db.Create(&u).Error
    if err != nil {
        return &User{}, err
    }
    return u, nil
}

func (u *User) SavePassword() error {
    //turn password into hash
    hashedPassword, errPassword := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
    if errPassword != nil {
        return  errPassword
    }
    u.Password = string(hashedPassword)
    return nil
}
