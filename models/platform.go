package models

type Platform struct{
	ID          		uint      	`gorm:"primary_key" json:"id"`
	Name    			string    	`json:"name"`
	GamePlatforms		[]GamePlatform		`json:"game_platform"`
}