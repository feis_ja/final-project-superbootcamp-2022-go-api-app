package models

type Wishlist struct {
	ID          	uint      		`gorm:"primary_key" json:"id"`
	UserID      	int  	  		`json:"user_id"`
	GameID      	int  			`json:"game_id"`
	User			User			`json:"user"`
	Game			Game			`json:"game"`


}