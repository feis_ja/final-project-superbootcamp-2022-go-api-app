package models

type GameImage struct{
	ID          uint      	`gorm:"primary_key" json:"id"`
	GameID      int      	`json:"game_id"`
	Image       string     	`json:"image"`
	Name       string     	`json:"name"`
	
} 
