package models

type GamePlatform struct {
	ID          	uint      		`gorm:"primary_key" json:"id"`
	GameID      	int  			`json:"game_id"`
	PlatformID      int  	  		`json:"platform_id"`
	MinimOS			string			`json:"minim_os"`
	MinimProcess	string			`json:"minim_process"`
	MinimDirectX	string			`json:"minim_directx"`
	MinimMemory		string			`json:"minim_memory"`
	MinimGraphics	string			`json:"minim_graphics"`
	MinimStorage	string			`json:"minim_Storage"`
	RecommeOS		string			`json:"recomme_OS"`
	RecommeProcess	string			`json:"recomme_process"`
	RecommeDirectX	string			`json:"recomme_directx"`
	RecommeMemory	string			`json:"recomme_memory"`
	RecommeGraphics	string			`json:"recomme_graphics"`
	RecommeStorage	string			`json:"recomme_storage"`
	LangSupport		string			`json:"lang_support"`
	Copyright		string			`json:"copyright"`
	Game			Game			`json:"game"`
	Platform		Platform		`json:"platform"`
	
}