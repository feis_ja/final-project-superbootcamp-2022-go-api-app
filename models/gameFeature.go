package models


type GameFeature struct {
	ID          	uint      		`gorm:"primary_key" json:"id"`
	GameID      	int  			`json:"game_id"`
	FeatureID      	int  	  		`json:"feature_id"`
	// mig
	Game			Game			`json:"game"`
	Feature			Feature			`json:"feature"`
}