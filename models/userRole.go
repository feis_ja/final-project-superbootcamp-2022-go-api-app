package models

type UserRole struct{
	ID          uint      `gorm:"primary_key" json:"id"`
	Name       string     `json:"name"`
	Users		[]User    `json:"-"`
} 
