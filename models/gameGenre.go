package models


type GameGenre struct {
	ID          	uint      		`gorm:"primary_key" json:"id"`
	GameID      	int  			`json:"game_id"`
	GenreID      	int  	  		`json:"genre_id"`
	// mig
	Game			Game			`json:"game"`
	Genre			Genre			`json:"genre"`


}