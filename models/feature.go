package models

type Feature struct{
	ID          	uint      			`gorm:"primary_key" json:"id"`
	Name       		string    			`json:"name"`
	// 
	GameFeatures	[]GameFeature		
}
