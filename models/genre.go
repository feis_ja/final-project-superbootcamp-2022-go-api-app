package models

type Genre struct{
	ID          	uint      `gorm:"primary_key" json:"id"`
	Name       		string    `json:"name"`
	GameGenres		[]GameGenre		
}
 