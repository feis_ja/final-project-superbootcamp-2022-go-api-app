package routes

import (
	"github.com/gin-contrib/cors"
    "github.com/gin-gonic/gin"
    "gorm.io/gorm"

    "api-app/controllers"
	"api-app/middlewares"

    swaggerFiles "github.com/swaggo/files"     // swagger embed files
    ginSwagger "github.com/swaggo/gin-swagger" // gin-swagger middleware
)

func SetupRouter(db *gorm.DB) *gin.Engine {
    r := gin.Default()

	corsConfig := cors.DefaultConfig()
	corsConfig.AllowAllOrigins = true
	corsConfig.AllowHeaders = []string{"Content-Type", "X-XSRF-TOKEN", "Accept", "Origin", "X-Requested-With", "Authorization"}

	// To be able to send tokens to the server.
	corsConfig.AllowCredentials = true

	// OPTIONS method for ReactJS
	corsConfig.AddAllowMethods("OPTIONS")

	r.Use(cors.New(corsConfig))


    // set db to gin context
    r.Use(func(c *gin.Context) {
        c.Set("db", db)
    })
  
    // AUTH
    r.POST("/register", controllers.Register)
    r.POST("/login", controllers.Login)
    userMiddlewareRoute := r.Group("/user")
    userMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
    userMiddlewareRoute.PATCH("/:id", controllers.UserUpdate)

	

	

	
   
    

   

   


    // GAME
    r.GET("/game", controllers.GetAllGame)
    r.GET("/game/:id", controllers.GetGameById)
    // -------
	gameMiddlewareRoute := r.Group("/game")
	gameMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
    gameMiddlewareRoute.POST("/", controllers.CreateGame)
    gameMiddlewareRoute.PATCH("/:id", controllers.UpdateGame)
    gameMiddlewareRoute.DELETE("/:id", controllers.DeleteGame)

    // FEATURE
    r.GET("/feature", controllers.GetAllFeature)
	r.GET("/feature/:id", controllers.GetFeatureById)
    // -------
    featureMiddlewareRoute := r.Group("/feature")
	featureMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
    featureMiddlewareRoute.POST("/", controllers.CreateFeature)
    featureMiddlewareRoute.PATCH("/:id", controllers.UpdateFeature)
    featureMiddlewareRoute.DELETE("/:id", controllers.DeleteFeature)
    
    // GENRE
    r.GET("/genre", controllers.GetAllGenre)
	r.GET("/genre/:id", controllers.GetGenreById)
    // -------
    genreMiddlewareRoute := r.Group("/genre")
	genreMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
    genreMiddlewareRoute.POST("/", controllers.CreateGenre)
    genreMiddlewareRoute.PATCH("/:id", controllers.UpdateGenre)
    genreMiddlewareRoute.DELETE("/:id", controllers.DeleteGenre)


    // PLATFORM
    r.GET("/platform", controllers.GetAllPlatform)
    r.GET("/platform/:id", controllers.GetPlatformById)
    // -------
    platformMiddlewareRoute := r.Group("/platform")
	platformMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
    platformMiddlewareRoute.POST("/", controllers.CreatePlatform)
    platformMiddlewareRoute.PATCH("/:id", controllers.UpdatePlatform)
    platformMiddlewareRoute.DELETE("/:id", controllers.DeletePlatform)

  
    // GAME IMAGE
    r.GET("/game_image", controllers.GetAllGameImage)
    r.GET("/game_image/:id", controllers.GetGameImageByGameId)
    r.GET("/game_image_one/:id", controllers.GetGameImageById)
    // -------
    gameImageMiddlewareRoute := r.Group("/game_image")
    gameImageMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
    gameImageMiddlewareRoute.POST("/", controllers.CreateGameImage)
    gameImageMiddlewareRoute.PATCH("/:id", controllers.UpdateGameImage)
    gameImageMiddlewareRoute.DELETE("/:id", controllers.DeleteGameImage)

    // GAME FEATURE
    r.GET("/game_feature", controllers.GetAllGameFeature)
    r.GET("/game_feature/:id", controllers.GetGameFeatureByGameId )
    r.GET("/game_feature_one/:id", controllers.GetGameFeatureById )
    //  ------
    gameFeaturesMiddlewareRoute := r.Group("/game_feature")
	gameFeaturesMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
    gameFeaturesMiddlewareRoute.POST("/", controllers.CreateGameFeature)
    gameFeaturesMiddlewareRoute.PATCH("/:id", controllers.UpdateGameFeature)
    gameFeaturesMiddlewareRoute.DELETE("/:id", controllers.DeleteGameFeature)

     // GAME GENRE
     r.GET("/game_genre", controllers.GetAllGameGenre)
     r.GET("/game_genre/:id", controllers.GetGameGenreByGameId )
     r.GET("/game_genre_one/:id", controllers.GetGameGenreById )
    //  ------
    gameGenreMiddlewareRoute := r.Group("/game_genre")
	gameGenreMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
    gameGenreMiddlewareRoute.POST("/", controllers.CreateGameGenre)
    gameGenreMiddlewareRoute.PATCH("/:id", controllers.UpdateGameGenre)
    gameGenreMiddlewareRoute.DELETE("/:id", controllers.DeleteGameGenre)

 
     // GAME PLATFORM
     r.GET("/game_platform", controllers.GetAllGamePlatform)
     r.GET("/game_platform/:id", controllers.GetGameGenreByGameId )
     r.GET("/game_platform_one/:id", controllers.GetGameGenreById )
    //  ------
    gamePLatformMiddlewareRoute := r.Group("/game_platform")
	gamePLatformMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
    gamePLatformMiddlewareRoute.POST("/", controllers.CreateGamePlatform)
    gamePLatformMiddlewareRoute.PATCH("/:id", controllers.UpdateGamePlatform)
    gamePLatformMiddlewareRoute.DELETE("/:id", controllers.DeleteGamePlatform)

    
     // PURCHASE
     r.GET("/purchase", controllers.GetAllPurchase )
     r.GET("/purchase/:id", controllers.GetPurchaseByUserId )
     r.GET("/purchase_one/:id", controllers.GetPurchaseById )
    //  ------
    purchaseMiddlewareRoute := r.Group("/purchase")
	purchaseMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
    purchaseMiddlewareRoute.POST("/", controllers.CreatePurchase)
    purchaseMiddlewareRoute.PATCH("/:id", controllers.UpdatePurchase)
    purchaseMiddlewareRoute.DELETE("/:id", controllers.DeletePurchase)

     
     // WISHLIST
     r.GET("/wishlist", controllers.GetAllWishlist )
     r.GET("/wishlist/:id", controllers.GetWishlistByUserId )
     r.GET("/wishlist_one/:id", controllers.GetWishlistById )
    //  ------
    wishlistMiddlewareRoute := r.Group("/wishlist")
	wishlistMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
    wishlistMiddlewareRoute.POST("/", controllers.CreateWishlist)
    wishlistMiddlewareRoute.PATCH("/:id", controllers.UpdateWishlist)
    wishlistMiddlewareRoute.DELETE("/:id", controllers.DeleteWishlist)



    r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

    return r 
}