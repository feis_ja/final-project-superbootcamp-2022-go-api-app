package main

import (
	"api-app/config"
	"api-app/docs"
	"api-app/routes"
	"api-app/utils"
	"log"

	"github.com/joho/godotenv"
)

// @contact.name API Support
// @contact.url http://www.swagger.io/support
// @contact.email support@swagger.io

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @termsOfService http://swagger.io/terms/

func main()  {
	// for load godotenv
	environment := utils.Getenv("ENVIRONMENT", "development")

    if environment == "development" {
      err := godotenv.Load()
      if err != nil {
        log.Fatal("Error loading .env file")
      }
    }

	//programmatically set swagger info
	docs.SwaggerInfo.Title = "Game Review API"
	docs.SwaggerInfo.Description = "This is a  server for app."
	docs.SwaggerInfo.Version = "1.0"
	docs.SwaggerInfo.Host = utils.Getenv("SWAGGER_HOST", "localhost:8080")
	docs.SwaggerInfo.Schemes = []string{"http", "https"}

	db := config.ConnectDataBase()
	sqlDB, _ := db.DB()
	defer sqlDB.Close()

	r := routes.SetupRouter(db)
	r.Run()
}