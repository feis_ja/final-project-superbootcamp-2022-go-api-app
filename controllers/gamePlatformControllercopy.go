package controllers

import (
	// "fmt"
	"net/http"

	// "time"

	"api-app/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type gamePlatformInput struct{
	PlatformID      int  	  		`json:"platform_id"`
	MinimOS			string			`json:"minim_os"`
	MinimProcess	string			`json:"minim_process"`
	MinimDirectX	string			`json:"minim_directx"`
	MinimMemory		string			`json:"minim_memory"`
	MinimGraphics	string			`json:"minim_graphics"`
	RecommeOS		string			`json:"recomme_OS"`
	RecommeProcess	string			`json:"recomme_process"`
	RecommeDirectX	string			`json:"recomme_directx"`
	RecommeMemory	string			`json:"recomme_memory"`
	RecommeGraphics	string			`json:"recomme_graphics"`
	LangSupport		string			`json:"lang_support"`
}

// GetAllGamePlatforms godoc
// @Summary Get All Game Platforms.
// @Description Get a List of Game Platforms.
// @Tags Game Platform
// @Produce json
// @Success 200 {object} []models.GamePlatform
// @Router /game_platform [get]
func GetAllGamePlatform(c *gin.Context) {
    // get db from gin context
    db := c.MustGet("db").(*gorm.DB)
    var gamePlatforms []models.GamePlatform
    db.Find(&gamePlatforms)

    c.JSON(http.StatusOK, gin.H{"data": gamePlatforms})
}

// CreateGamePlatform godoc
// @Summary Create New Game Platform.
// @Description Creating a New Game Platform.
// @Tags Game Platform
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Param Body body gameInput true "the body to create a new game platform"
// @Produce json
// @Success 200 {object} gamePlatformInput
// @Router /game_platform [post]
func CreateGamePlatform(c *gin.Context) {
	// Validate input
	var input gamePlatformInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create Game
	game := gamePlatformInput{
		PlatformID      :input.PlatformID,
		MinimOS			:input.MinimOS,			
		MinimProcess	:input.MinimProcess,			
		MinimDirectX	:input.MinimDirectX,			
		MinimMemory		:input.MinimMemory,			
		MinimGraphics	:input.MinimGraphics,			
		RecommeOS		:input.RecommeOS,			
		RecommeProcess	:input.RecommeProcess,			
		RecommeDirectX	:input.RecommeDirectX,			
		RecommeMemory	:input.RecommeMemory,			
		RecommeGraphics	:input.RecommeGraphics,			
		LangSupport		:input.LangSupport,			
	}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&game)

	c.JSON(http.StatusOK, gin.H{"data": game})
}

// GetGamePlatformById godoc
// @Summary Get Game Platform.
// @Description Get an Game Platform by Id.
// @Tags Game Platform
// @Produce json
// @Param id path string true "Game Platform id"
// @Success 200 {object} models.GamePlatform
// @Router /game_platform_one/{id} [get]
func GetGamePlatformById(c *gin.Context) { // Get model if exist
    var game models.GamePlatform

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", c.Param("id")).First(&game).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": game})
}
// GetGamePlatformByGameId godoc
// @Summary Get Game Platform by Game Id.
// @Description Get an Game Platform by Game Id.
// @Tags Game Platform
// @Produce json
// @Param id path string true "Game id"
// @Success 200 {object} gamePlatformInput
// @Router /game_platform/{id} [get]
func GetGamePlatformByGameId(c *gin.Context) { // Get model if exist
	var games []models.GamePlatform
	db := c.MustGet("db").(*gorm.DB)
	db.Preload("GamePlatforms.Platform").Preload(clause.Associations).Where("id = ?", c.Param("id")).Find(&games)

	c.JSON(http.StatusOK, gin.H{"data": games})
}

// UpdateGamePlatform godoc
// @Summary Update Game Platform.
// @Description Update Game Platform by Id.
// @Tags Game Platform
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "Game Platform id"
// @Param Body body gameInput true "the body to update game Platform"
// @Success 200 {object} models.GamePlatform
// @Router /game_platform/{id} [patch]
func UpdateGamePlatform(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    // Get model if exist
    var gamePlatform models.GamePlatform
    if err := db.Where("id = ?", c.Param("id")).First(&gamePlatform).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    // Validate input
    var input gamePlatformInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    var updatedInput models.GamePlatform
	updatedInput.PlatformID= input.PlatformID
	updatedInput.MinimOS= input.MinimOS			
	updatedInput.MinimProcess= input.MinimProcess			
	updatedInput.MinimDirectX= input.MinimDirectX			
	updatedInput.MinimMemory= input.MinimMemory			
	updatedInput.MinimGraphics= input.MinimGraphics			
	updatedInput.RecommeOS= input.RecommeOS			
	updatedInput.RecommeProcess= input.RecommeProcess			
	updatedInput.RecommeDirectX= input.RecommeDirectX			
	updatedInput.RecommeMemory= input.RecommeMemory			
	updatedInput.RecommeGraphics= input.RecommeGraphics			
	updatedInput.LangSupport= input.LangSupport	

    db.Model(&gamePlatform).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"data": gamePlatform})
}

// DeleteGamePlatform godoc
// @Summary Delete One Game Platform.
// @Description Delete Batch Game Platform by Id.
// @Tags Game Platform
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "Game Platform id"
// @Success 200 {object} map[string]boolean
// @Router /game_platform/{id} [delete]
func DeleteGamePlatform(c *gin.Context) {
    // Get model if exist
    db := c.MustGet("db").(*gorm.DB)
    var gamePlatform models.GamePlatform
    if err := db.Where("id = ?", c.Param("id")).First(&gamePlatform).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    db.Delete(&gamePlatform)

    c.JSON(http.StatusOK, gin.H{"data": true})
}