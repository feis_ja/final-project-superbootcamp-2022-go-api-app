package controllers

import (
    "net/http"
    // "time"

    "api-app/models"

    "github.com/gin-gonic/gin"
    "gorm.io/gorm"
)

type featureInput struct {
	Name       		string    			`json:"name"`
}

// GetAllFeatures godoc
// @Summary Get All Features.
// @Description Get a list of features.
// @Tags feature
// @Produce json
// @Success 200 {object} []models.Feature
// @Router /feature [get]
func GetAllFeature(c *gin.Context) {
    // get db from gin context
    db := c.MustGet("db").(*gorm.DB)
    var features []models.Feature
    db.Find(&features)

    c.JSON(http.StatusOK, gin.H{"data": features})
}

// CreateFeature godoc
// @Summary Create New Feature.
// @Description Creating a new feature.
// @Tags feature
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Param Body body featureInput true "the body to create a new feature"
// @Produce json
// @Success 200 {object} models.Feature
// @Router /feature [post]
func CreateFeature(c *gin.Context) {
    // Validate input
    var input featureInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    // Create feature
    feature := models.Feature{
		Name: input.Name,
    }
    db := c.MustGet("db").(*gorm.DB)
    db.Create(&feature)

    c.JSON(http.StatusOK, gin.H{"data": feature})
}

// GetFeatureById godoc
// @Summary Get Feature.
// @Description Get an feature by id.
// @Tags feature
// @Produce json
// @Param id path string true "feature id"
// @Success 200 {object} models.Feature
// @Router /feature/{id} [get]
func GetFeatureById(c *gin.Context) { // Get model if exist
    var feature models.Feature

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", c.Param("id")).First(&feature).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": feature})
}


// UpdateFeature godoc
// @Summary Update Feature.
// @Description Update feature by id.
// @Tags feature
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "feature id"
// @Param Body body featureInput true "the body to update feature"
// @Success 200 {object} models.Feature
// @Router /feature/{id} [patch]
func UpdateFeature(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    // Get model if exist
    var feature models.Feature
    if err := db.Where("id = ?", c.Param("id")).First(&feature).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    // Validate input
    var input featureInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    var updatedInput models.Feature
	updatedInput.Name = input.Name					

    db.Model(&feature).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"data": feature})
}

// DeleteFeature godoc
// @Summary Delete One Feature.
// @Description Delete a feature by id.
// @Tags feature
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "feature id"
// @Success 200 {object} map[string]boolean
// @Router /feature/{id} [delete]
func DeleteFeature(c *gin.Context) {
    // Get model if exist
    db := c.MustGet("db").(*gorm.DB)
    var feature models.Feature
    if err := db.Where("id = ?", c.Param("id")).First(&feature).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    db.Delete(&feature)

    c.JSON(http.StatusOK, gin.H{"data": true})
}