package controllers

import (
    "net/http"
    // "time"

    "api-app/models"

    "github.com/gin-gonic/gin"
    "gorm.io/gorm"
)

type platformInput struct {
	Name       		string    			`json:"name"`
}

// GetAllPlatforms godoc
// @Summary Get All Platforms.
// @Description Get a list of platforms.
// @Tags platform
// @Produce json
// @Success 200 {object} []models.Platform
// @Router /platform [get]
func GetAllPlatform(c *gin.Context) {
    // get db from gin context
    db := c.MustGet("db").(*gorm.DB)
    var platforms []models.Platform
    db.Find(&platforms)

    c.JSON(http.StatusOK, gin.H{"data": platforms})
}

// CreatePlatform godoc
// @Summary Create New Platform.
// @Description Creating a new platform.
// @Tags platform
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Param Body body platformInput true "the body to create a new platform"
// @Produce json
// @Success 200 {object} models.Platform
// @Router /platform [post]
func CreatePlatform(c *gin.Context) {
    // Validate input
    var input platformInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    // Create platform
    platform := models.Platform{
		Name: input.Name,
    }
    db := c.MustGet("db").(*gorm.DB)
    db.Create(&platform)

    c.JSON(http.StatusOK, gin.H{"data": platform})
}

// GetPlatformById godoc
// @Summary Get Platform.
// @Description Get an platform by id.
// @Tags platform
// @Produce json
// @Param id path string true "platform id"
// @Success 200 {object} models.Platform
// @Router /platform/{id} [get]
func GetPlatformById(c *gin.Context) { // Get model if exist
    var platform models.Platform

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", c.Param("id")).First(&platform).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": platform})
}


// UpdatePlatform godoc
// @Summary Update Platform.
// @Description Update platform by id.
// @Tags platform
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "platform id"
// @Param Body body platformInput true "the body to update platform"
// @Success 200 {object} models.Platform
// @Router /platform/{id} [patch]
func UpdatePlatform(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    // Get model if exist
    var platform models.Platform
    if err := db.Where("id = ?", c.Param("id")).First(&platform).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    // Validate input
    var input platformInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    var updatedInput models.Platform
	updatedInput.Name = input.Name					

    db.Model(&platform).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"data": platform})
}

// DeletePlatform godoc
// @Summary Delete One platform.
// @Description Delete a platform by id.
// @Tags platform
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "platform id"
// @Success 200 {object} map[string]boolean
// @Router /platform/{id} [delete]
func DeletePlatform(c *gin.Context) {
    // Get model if exist
    db := c.MustGet("db").(*gorm.DB)
    var platform models.Platform
    if err := db.Where("id = ?", c.Param("id")).First(&platform).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    db.Delete(&platform)

    c.JSON(http.StatusOK, gin.H{"data": true})
}