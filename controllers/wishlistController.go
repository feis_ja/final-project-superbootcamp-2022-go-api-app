package controllers

import (
	// "fmt"
	"net/http"

	// "time"

	"api-app/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type wishlistInput struct{
	UserID      	int  	  		`json:"user_id"`
	GameID      	int  			`json:"game_id"`
}

// GetAllWishlist godoc
// @Summary Get All Game Wishlists.
// @Description Get a List of Game Wishlists.
// @Tags Game Wishlist
// @Produce json
// @Success 200 {object} []models.Wishlist
// @Router /wishlist [get]
func GetAllWishlist(c *gin.Context) {
    // get db from gin context
    db := c.MustGet("db").(*gorm.DB)
    var Wishlists []models.Wishlist
    db.Find(&Wishlists)

    c.JSON(http.StatusOK, gin.H{"data": Wishlists})
}

// CreateWishlist godoc
// @Summary Create New Game Wishlist.
// @Description Creating a New Game Wishlist.
// @Tags Game Wishlist
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Param Body body gameInput true "the body to create a new game Wishlist"
// @Produce json
// @Success 200 {object} wishlistInput
// @Router /wishlist [post]
func CreateWishlist(c *gin.Context) {
	// Validate input
	var input wishlistInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create Game
	game := wishlistInput{
		GameID:    input.GameID,
		UserID: 	input.UserID,
	}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&game)

	c.JSON(http.StatusOK, gin.H{"data": game})
}

// GetWishlistById godoc
// @Summary Get Game Wishlist.
// @Description Get an Game Wishlist by Id.
// @Tags Game Wishlist
// @Produce json
// @Param id path string true "Game Wishlist id"
// @Success 200 {object} models.Wishlist
// @Router /wishlist_one/{id} [get]
func GetWishlistById(c *gin.Context) { // Get model if exist
    var game models.Wishlist

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", c.Param("id")).First(&game).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": game})
}

// GetWishlistByUserId godoc
// @Summary Get Game Wishlist by User Id.
// @Description Get an Game Wishlist by User Id.
// @Tags Game Wishlist
// @Produce json
// @Param id path string true "User id"
// @Success 200 {object} wishlistInput
// @Router /wishlist/{id} [get]
func GetWishlistByUserId(c *gin.Context) { // Get model if exist
	var user []models.User
	db := c.MustGet("db").(*gorm.DB)
	db.Preload("Wishlists.Game").Preload(clause.Associations).Where("id = ?", c.Param("id")).Find(&user)

	c.JSON(http.StatusOK, gin.H{"data": user})
}

// UpdateWishlist godoc
// @Summary Update Game Wishlist.
// @Description Update Game Wishlist by Id.
// @Tags Game Wishlist
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "Game Wishlist id"
// @Param Body body gameInput true "the body to update game Wishlist"
// @Success 200 {object} models.Wishlist
// @Router /wishlist/{id} [patch]
func UpdateWishlist(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    // Get model if exist
    var Wishlist models.Wishlist
    if err := db.Where("id = ?", c.Param("id")).First(&Wishlist).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    // Validate input
    var input wishlistInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    var updatedInput models.Wishlist
	updatedInput.GameID = input.GameID
	updatedInput.UserID = input.UserID

    db.Model(&Wishlist).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"data": Wishlist})
}

// DeleteWishlist godoc
// @Summary Delete One Game Wishlist.
// @Description Delete Batch Game Wishlist by Id.
// @Tags Game Wishlist
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "Game Wishlist id"
// @Success 200 {object} map[string]boolean
// @Router /wishlist/{id} [delete]
func DeleteWishlist(c *gin.Context) {
    // Get model if exist
    db := c.MustGet("db").(*gorm.DB)
    var Wishlist models.Wishlist
    if err := db.Where("id = ?", c.Param("id")).First(&Wishlist).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    db.Delete(&Wishlist)

    c.JSON(http.StatusOK, gin.H{"data": true})
}
