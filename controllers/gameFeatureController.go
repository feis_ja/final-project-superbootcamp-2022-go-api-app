package controllers

import (
	// "fmt"
	"net/http"

	// "time"

	"api-app/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type gameFeatureInput struct {
	GameID    int    `json:"game_id"`
	FeatureID int    `json:"feature_id"`
}



// GetAllGameFeatures godoc
// @Summary Get All Game Features.
// @Description Get a list of game features.
// @Tags Game Feature
// @Produce json
// @Success 200 {object} []models.GameFeature
// @Router /game_feature [get]
func GetAllGameFeature(c *gin.Context) {
    // get db from gin context
    db := c.MustGet("db").(*gorm.DB)
    var gameFeatures []models.GameFeature
    db.Find(&gameFeatures)

    c.JSON(http.StatusOK, gin.H{"data": gameFeatures})
}

// CreateGameFeature godoc
// @Summary Create New Game Feature.
// @Description Creating a new name feature.
// @Tags Game Feature
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Param Body body gameInput true "the body to create a new game feature"
// @Produce json
// @Success 200 {object} gameFeatureInput
// @Router /game_feature [post]
func CreateGameFeature(c *gin.Context) {
	// Validate input
	var input gameFeatureInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create Game
	game := gameFeatureInput{
		GameID:    input.GameID,
		FeatureID: input.FeatureID,
	}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&game)

	c.JSON(http.StatusOK, gin.H{"data": game})
}

// GetGameFeatureById godoc
// @Summary Get Game Feature.
// @Description Get an Game feature by id.
// @Tags Game Feature
// @Produce json
// @Param id path string true "Game Feature id"
// @Success 200 {object} models.GameFeature
// @Router /game_feature_one/{id} [get]
func GetGameFeatureById(c *gin.Context) { // Get model if exist
    var game models.GameFeature

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", c.Param("id")).First(&game).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": game})
}

// GetGameFeatureByGameId godoc
// @Summary Get Game Feature by Game Id.
// @Description Get an Game Feature by game id.
// @Tags Game Feature
// @Produce json
// @Param id path string true "Game id"
// @Success 200 {object} gameFeatureInput
// @Router /game_feature/{id} [get]
func GetGameFeatureByGameId(c *gin.Context) { // Get model if exist
	var games []models.Game
	db := c.MustGet("db").(*gorm.DB)
	// db.Preload("GameFeatures.Feature").Preload(clause.Associations).Find(&games)
	db.Preload("GameFeatures.Feature").Preload(clause.Associations).Where("id = ?", c.Param("id")).Find(&games)
	// db.Preload("GameFeatures.GameGenres.Genre.Feature").Preload(clause.Associations).Where("id = ?", c.Param("id")).Find(&games)

	// fmt.Println("ini data games>>>>>", games)

	c.JSON(http.StatusOK, gin.H{"data": games})
}

// UpdateGameFeature godoc
// @Summary Update Game Feature.
// @Description Update Game feature by id.
// @Tags Game Feature
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "Game Feature id"
// @Param Body body gameInput true "the body to update game feature"
// @Success 200 {object} models.GameFeature
// @Router /game_feature/{id} [patch]
func UpdateGameFeature(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    // Get model if exist
    var gameFeature models.GameFeature
    if err := db.Where("id = ?", c.Param("id")).First(&gameFeature).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    // Validate input
    var input gameFeatureInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    var updatedInput models.GameFeature
	updatedInput.GameID = input.GameID
	updatedInput.FeatureID = input.FeatureID

    db.Model(&gameFeature).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"data": gameFeature})
}

// DeleteGameFeature godoc
// @Summary Delete One Game Feature.
// @Description Delete Batch Game feature by id.
// @Tags Game Feature
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "Game Feature id"
// @Success 200 {object} map[string]boolean
// @Router /game_feature/{id} [delete]
func DeleteGameFeature(c *gin.Context) {
    // Get model if exist
    db := c.MustGet("db").(*gorm.DB)
    var gameFeature models.GameFeature
    if err := db.Where("id = ?", c.Param("id")).First(&gameFeature).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    db.Delete(&gameFeature)

    c.JSON(http.StatusOK, gin.H{"data": true})
}
