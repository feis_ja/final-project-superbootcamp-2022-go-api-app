package controllers

import (
	// "fmt"
	"net/http"

	// "time"

	"api-app/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type gameGenreInput struct {
	GameID   	int    `json:"game_id"`
	GenreID 	int    `json:"genre_id"`

}

// GetAllGameGenres godoc
// @Summary Get All Game Genres.
// @Description Get a List of Game Genres.
// @Tags Game Genre
// @Produce json
// @Success 200 {object} []models.GameGenre
// @Router /game_genre [get]
func GetAllGameGenre(c *gin.Context) {
    // get db from gin context
    db := c.MustGet("db").(*gorm.DB)
    var gameGenres []models.GameGenre
    db.Find(&gameGenres)

    c.JSON(http.StatusOK, gin.H{"data": gameGenres})
}

// CreateGameGenre godoc
// @Summary Create New Game Genre.
// @Description Creating a New Game Genre.
// @Tags Game Genre
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Param Body body gameInput true "the body to create a new game genre"
// @Produce json
// @Success 200 {object} gameGenreInput
// @Router /game_genre [post]
func CreateGameGenre(c *gin.Context) {
	// Validate input
	var input gameGenreInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create Game
	game := gameGenreInput{
		GameID:    input.GameID,
		GenreID: 	input.GenreID,
	}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&game)

	c.JSON(http.StatusOK, gin.H{"data": game})
}

// GetGameGenreById godoc
// @Summary Get Game Genre.
// @Description Get an Game Genre by Id.
// @Tags Game Genre
// @Produce json
// @Param id path string true "Game Genre id"
// @Success 200 {object} models.GameGenre
// @Router /game_genre_one/{id} [get]
func GetGameGenreById(c *gin.Context) { // Get model if exist
    var game models.GameGenre

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", c.Param("id")).First(&game).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": game})
}
// GetGameGenreByGameId godoc
// @Summary Get Game Genre by Game Id.
// @Description Get an Game Genre by by Game Id.
// @Tags Game Genre
// @Produce json
// @Param id path string true "Game id"
// @Success 200 {object} gameGenreInput
// @Router /game_genre/{id} [get]
func GetGameGenreByGameId(c *gin.Context) { // Get model if exist
	var games []models.Game
	db := c.MustGet("db").(*gorm.DB)
	db.Preload("GameGenres.Genre").Preload(clause.Associations).Where("id = ?", c.Param("id")).Find(&games)

	c.JSON(http.StatusOK, gin.H{"data": games})
}

// UpdateGameGenre godoc
// @Summary Update Game Genre.
// @Description Update Game Genre by Id.
// @Tags Game Genre
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "Game Genre id"
// @Param Body body gameInput true "the body to update game genre"
// @Success 200 {object} models.GameGenre
// @Router /game_genre/{id} [patch]
func UpdateGameGenre(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    // Get model if exist
    var gameGenre models.GameGenre
    if err := db.Where("id = ?", c.Param("id")).First(&gameGenre).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    // Validate input
    var input gameGenreInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    var updatedInput models.GameGenre
	updatedInput.GameID = input.GameID
	updatedInput.GenreID = input.GenreID

    db.Model(&gameGenre).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"data": gameGenre})
}

// DeleteGameGenre godoc
// @Summary Delete One Game Genre.
// @Description Delete Batch Game Genre by Id.
// @Tags Game Genre
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "Game Genre id"
// @Success 200 {object} map[string]boolean
// @Router /game_genre/{id} [delete]
func DeleteGameGenre(c *gin.Context) {
    // Get model if exist
    db := c.MustGet("db").(*gorm.DB)
    var gameGenre models.GameGenre
    if err := db.Where("id = ?", c.Param("id")).First(&gameGenre).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    db.Delete(&gameGenre)

    c.JSON(http.StatusOK, gin.H{"data": true})
}