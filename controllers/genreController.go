package controllers

import (
    "net/http"
    // "time"

    "api-app/models"

    "github.com/gin-gonic/gin"
    "gorm.io/gorm"
)

type genreInput struct {
	Name       		string    			`json:"name"`
}

// GetAllGenres godoc
// @Summary Get All Genres.
// @Description Get a list of genres.
// @Tags genre
// @Produce json
// @Success 200 {object} []models.Genre
// @Router /genre [get]
func GetAllGenre(c *gin.Context) {
    // get db from gin context
    db := c.MustGet("db").(*gorm.DB)
    var genres []models.Genre
    db.Find(&genres)

    c.JSON(http.StatusOK, gin.H{"data": genres})
}

// CreateGenre godoc
// @Summary Create New Genre.
// @Description Creating a new genre.
// @Tags genre
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Param Body body genreInput true "the body to create a new genre"
// @Produce json
// @Success 200 {object} models.Genre
// @Router /genre [post]
func CreateGenre(c *gin.Context) {
    // Validate input
    var input genreInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    // Create genre
    genre := models.Genre{
		Name: input.Name,
    }
    db := c.MustGet("db").(*gorm.DB)
    db.Create(&genre)

    c.JSON(http.StatusOK, gin.H{"data": genre})
}

// GetGenreById godoc
// @Summary Get Genre.
// @Description Get an genre by id.
// @Tags genre
// @Produce json
// @Param id path string true "genre id"
// @Success 200 {object} models.Genre
// @Router /genre/{id} [get]
func GetGenreById(c *gin.Context) { // Get model if exist
    var genre models.Genre

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", c.Param("id")).First(&genre).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": genre})
}


// UpdateGenre godoc
// @Summary Update Genre.
// @Description Update genre by id.
// @Tags genre
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "genre id"
// @Param Body body genreInput true "the body to update genre"
// @Success 200 {object} models.Genre
// @Router /genre/{id} [patch]
func UpdateGenre(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    // Get model if exist
    var genre models.Genre
    if err := db.Where("id = ?", c.Param("id")).First(&genre).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    // Validate input
    var input genreInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    var updatedInput models.Genre
	updatedInput.Name = input.Name					

    db.Model(&genre).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"data": genre})
}

// DeleteGenre godoc
// @Summary Delete One Genre.
// @Description Delete a genre by id.
// @Tags genre
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "genre id"
// @Success 200 {object} map[string]boolean
// @Router /genre/{id} [delete]
func DeleteGenre(c *gin.Context) {
    // Get model if exist
    db := c.MustGet("db").(*gorm.DB)
    var genre models.Genre
    if err := db.Where("id = ?", c.Param("id")).First(&genre).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    db.Delete(&genre)

    c.JSON(http.StatusOK, gin.H{"data": true})
}