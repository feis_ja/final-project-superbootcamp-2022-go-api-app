package controllers

import (
    "net/http"
    // "time"

    "api-app/models"

    "github.com/gin-gonic/gin"
    "gorm.io/gorm"
    "gorm.io/gorm/clause"
)

type gameInput struct {
	Poster				string		`json:"poster"`
	PosterCaption		string		`json:"poster_caption"`
	Logo				string		`json:"logo"`
	Title       		string     	`json:"title"`
	Price       		uint       	`json:"price"`
	Developer      		string     	`json:"developer"`
	Publisher       	string    	`json:"publisher"`
	ReleaseDate 		string     	`json:"release_date"`
	ShortDescription	string		`json:"short_description"`
	Description			string		`json:"description"`
}

// GetAllGames godoc
// @Summary Get all games.
// @Description Get a list of Games.
// @Tags Game
// @Produce json
// @Success 200 {object} []models.Game
// @Router /game [get]
func GetAllGame(c *gin.Context) {
    // get db from gin context
    db := c.MustGet("db").(*gorm.DB)
    var games []models.Game
    // db.Find(&games)
	db.Preload("GameFeatures").Preload(clause.Associations).Find(&games)


    c.JSON(http.StatusOK, gin.H{"data": games})
}

// CreateGame godoc
// @Summary Create New Game.
// @Description Creating a new Game.
// @Tags Game
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Param Body body gameInput true "the body to create a new Game"
// @Produce json
// @Success 200 {object} models.Game
// @Router /game [post]
func CreateGame(c *gin.Context) {
    // Validate input
    var input gameInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    // Create Game
    game := models.Game{
        Poster : input.Poster,				
        PosterCaption : input.PosterCaption,	
        Logo : input.Logo,				
        Price : input.Price,     		
        Developer : input.Developer,		
        Publisher : input.Publisher,       	
        ReleaseDate : input.ReleaseDate,
        ShortDescription : input.ShortDescription,
        Description : input.Description,
    }
    db := c.MustGet("db").(*gorm.DB)
    db.Create(&game)

    c.JSON(http.StatusOK, gin.H{"data": game})
}

// GetGameById godoc
// @Summary Get Game.
// @Description Get an Game by id.
// @Tags Game
// @Produce json
// @Param id path string true "Game id"
// @Success 200 {object} models.Game
// @Router /game/{id} [get]
func GetGameById(c *gin.Context) { // Get model if exist
    var game models.Game

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", c.Param("id")).First(&game).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": game})
}


// UpdateGame godoc
// @Summary Update Game.
// @Description Update Game by id.
// @Tags Game
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "Game id"
// @Param Body body gameInput true "the body to update game"
// @Success 200 {object} models.Game
// @Router /game/{id} [patch]
func UpdateGame(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    // Get model if exist
    var game models.Game
    if err := db.Where("id = ?", c.Param("id")).First(&game).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    // Validate input
    var input gameInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    var updatedInput models.Game
	updatedInput.Poster = input.Poster				
	updatedInput.PosterCaption = input.PosterCaption		
	updatedInput.Logo = input.Logo				
	updatedInput.Price = input.Price       		
	updatedInput.Developer = input.Developer      		
	updatedInput.Publisher = input.Publisher       	
	updatedInput.ReleaseDate = input.ReleaseDate 		
	updatedInput.ShortDescription = input.ShortDescription	
	updatedInput.Description = input.Description			

    db.Model(&game).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"data": game})
}

// DeleteGame godoc
// @Summary Delete one Game.
// @Description Delete a Game by id.
// @Tags Game
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "Game id"
// @Success 200 {object} map[string]boolean
// @Router /game/{id} [delete]
func DeleteGame(c *gin.Context) {
    // Get model if exist
    db := c.MustGet("db").(*gorm.DB)
    var game models.Game
    if err := db.Where("id = ?", c.Param("id")).First(&game).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    db.Delete(&game)

    c.JSON(http.StatusOK, gin.H{"data": true})
}