package controllers

import (
    "net/http"
    // "time"

    "api-app/models"

    "github.com/gin-gonic/gin"
    "gorm.io/gorm"
    "gorm.io/gorm/clause"
)

type gameImageInput struct {
	GameID      int      	`json:"game_id"`
	Image       string     	`json:"image"`
    Name        string     	`json:"name"`	
}

// GetAllGameImages godoc
// @Summary Get All Game Images.
// @Description Get a List of Game Images.
// @Tags Game Image
// @Produce json
// @Success 200 {object} []models.GameImage
// @Router /game_image [get]
func GetAllGameImage(c *gin.Context) {
    // get db from gin context
    db := c.MustGet("db").(*gorm.DB)
    var gameImages []models.GameImage
    db.Find(&gameImages)

    c.JSON(http.StatusOK, gin.H{"data": gameImages})
}

// CreateGameImage godoc
// @Summary Create New Game Image.
// @Description Creating a New Game Image.
// @Tags Game Image
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Param Body body gameInput true "the body to create a new game image"
// @Produce json
// @Success 200 {object} models.GameImage
// @Router /game_image [post]
func CreateGameImage(c *gin.Context) {
    // Validate input
    var input gameImageInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    // Create Game
    game := models.GameImage{
		GameID: input.GameID,
		Image:  input.Image,
        Name: input.Name,   
    }
    db := c.MustGet("db").(*gorm.DB)
    db.Create(&game)

    c.JSON(http.StatusOK, gin.H{"data": game})
}

// GetGameImageById godoc
// @Summary Get Game Image.
// @Description Get an Game Image by Id.
// @Tags Game Image
// @Produce json
// @Param id path string true "Game Image id"
// @Success 200 {object} models.GameImage
// @Router /game_image_one/{id} [get]
func GetGameImageById(c *gin.Context) { // Get model if exist
    var gameImage models.GameImage

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", c.Param("id")).First(&gameImage).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": gameImage})
}

// GetGameImageByGameId godoc
// @Summary Get Game Image by Game Id.
// @Description Get an Game Image by by Game Id.
// @Tags Game Image
// @Produce json
// @Param id path string true "Game id"
// @Success 200 {object} gameImageInput
// @Router /game_image/{id} [get]
func GetGameImageByGameId(c *gin.Context) { // Get model if exist
	var games []models.Game
	db := c.MustGet("db").(*gorm.DB)
	db.Preload("GameImages").Preload(clause.Associations).Where("id = ?", c.Param("id")).Find(&games)

	c.JSON(http.StatusOK, gin.H{"data": games})
}


// UpdateGameImage godoc
// @Summary Update Game Image.
// @Description Update Game Image by Id.
// @Tags Game Image
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "Game Image id"
// @Param Body body gameInput true "the body to update game image"
// @Success 200 {object} models.GameImage
// @Router /game_image/{id} [patch]
func UpdateGameImage(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    // Get model if exist
    var gameImage models.GameImage
    if err := db.Where("id = ?", c.Param("id")).First(&gameImage).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    // Validate input
    var input gameImageInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    var updatedInput models.GameImage
	updatedInput.GameID = input.GameID	
	updatedInput.Image = input.Image
    updatedInput.Name = input.Name	
		

    db.Model(&gameImage).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"data": gameImage})
}

// DeleteGame Image godoc
// @Summary Delete one Game Image.
// @Description Delete Batch Game Image by Id.
// @Tags Game Image
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "Game Image id"
// @Success 200 {object} map[string]boolean
// @Router /game_image/{id} [delete]
func DeleteGameImage(c *gin.Context) {
    // Get model if exist
    db := c.MustGet("db").(*gorm.DB)
    var gameImage models.GameImage
    if err := db.Where("id = ?", c.Param("id")).First(&gameImage).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    db.Delete(&gameImage)

    c.JSON(http.StatusOK, gin.H{"data": true})
}