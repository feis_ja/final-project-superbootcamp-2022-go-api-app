package controllers

import (
	// "fmt"
	"net/http"

	// "time"

	"api-app/models"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type purchaseInput struct{
	UserID      	int  	  		`json:"user_id"`
	GameID      	int  			`json:"game_id"`
}

// GetAllPurchase godoc
// @Summary Get All Game Purchases.
// @Description Get a List of Game Purchases.
// @Tags Game Purchase
// @Produce json
// @Success 200 {object} []models.Purchase
// @Router /purchase [get]
func GetAllPurchase(c *gin.Context) {
    // get db from gin context
    db := c.MustGet("db").(*gorm.DB)
    var Purchases []models.Purchase
    db.Find(&Purchases)

    c.JSON(http.StatusOK, gin.H{"data": Purchases})
}

// CreatePurchase godoc
// @Summary Create New Game Purchase.
// @Description Creating a New Game Purchase.
// @Tags Game Purchase
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Param Body body gameInput true "the body to create a new game purchase"
// @Produce json
// @Success 200 {object} purchaseInput
// @Router /purchase [post]
func CreatePurchase(c *gin.Context) {
	// Validate input
	var input purchaseInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create Game
	game := purchaseInput{
		GameID:    input.GameID,
		UserID: 	input.UserID,
	}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&game)

	c.JSON(http.StatusOK, gin.H{"data": game})
}

// GetPurchaseById godoc
// @Summary Get Game Purchase.
// @Description Get an Game Purchase by Id.
// @Tags Game Purchase
// @Produce json
// @Param id path string true "Game Purchase id"
// @Success 200 {object} models.Purchase
// @Router /purchase_one/{id} [get]
func GetPurchaseById(c *gin.Context) { // Get model if exist
    var game models.Purchase

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", c.Param("id")).First(&game).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": game})
}
// GetPurchaseByUserId godoc
// @Summary Get Game Purchase by User Id.
// @Description Get an Game Purchase by User Id.
// @Tags Game Purchase
// @Produce json
// @Param id path string true "User Id"
// @Success 200 {object} purchaseInput
// @Router /purchase/{id} [get]
func GetPurchaseByUserId(c *gin.Context) { // Get model if exist
	var user []models.User
	db := c.MustGet("db").(*gorm.DB)
	db.Preload("Purchases.Game").Preload(clause.Associations).Where("id = ?", c.Param("id")).Find(&user)

	c.JSON(http.StatusOK, gin.H{"data": user})
}

// UpdatePurchase godoc
// @Summary Update Game Purchase.
// @Description Update Game Purchase by Id.
// @Tags Game Purchase
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "Game Purchase id"
// @Param Body body gameInput true "the body to update game Purchase"
// @Success 200 {object} models.Purchase
// @Router /purchase/{id} [patch]
func UpdatePurchase(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    // Get model if exist
    var Purchase models.Purchase
    if err := db.Where("id = ?", c.Param("id")).First(&Purchase).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    // Validate input
    var input purchaseInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    var updatedInput models.Purchase
	updatedInput.GameID = input.GameID
	updatedInput.UserID = input.UserID

    db.Model(&Purchase).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"data": Purchase})
}

// DeletePurchase godoc
// @Summary Delete One Game Purchase.
// @Description Delete Batch Game Purchase by Id.
// @Tags Game Purchase
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "Game Purchase id"
// @Success 200 {object} map[string]boolean
// @Router /purchase/{id} [delete]
func DeletePurchase(c *gin.Context) {
    // Get model if exist
    db := c.MustGet("db").(*gorm.DB)
    var Purchase models.Purchase
    if err := db.Where("id = ?", c.Param("id")).First(&Purchase).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    db.Delete(&Purchase)

    c.JSON(http.StatusOK, gin.H{"data": true})
}
